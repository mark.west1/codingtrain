package neuralnet

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

var (
	rng *rand.Rand
)

// Initialize the random number generator.
func init() {
	rng = rand.New(rand.NewSource(time.Now().UnixNano()))
}

// Run a simple neural network to guess if a point is on above or below a line
// in two-dimensional space.
func Run(iterationCount int, inputList [][]float64) {
	brain := NewNode(2)

	for _, inputs := range inputList {
		for i := 0; i < iterationCount; i++ {
			g, err := brain.Guess(inputs)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			fmt.Printf("guess %d: %d\n", i+1, g)
		}
	}
}
