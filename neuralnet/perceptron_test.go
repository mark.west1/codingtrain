package neuralnet

import "testing"

func TestGuess(t *testing.T) {
	data := []struct {
		inputs   []float64
		expected int
		isError  bool
	}{
		{
			[]float64{-1.0, 0.5},
			-1,
			false,
		},
	}

	for _, tt := range data {
		p := New(2)
		actual, err := p.Guess(tt.inputs)

		if tt.isError && err != nil {
			t.Errorf("expecting no error, got %v", err)
		} else if tt.expected != actual {
			t.Errorf("expected guess %d, got %d", tt.expected, actual)
		}
	}
}
