package neuralnet

import (
	"image/color"
)

// Point is a position in a two-dimensional cartesian coordinate system. Label
// indicates whether the point is above or below a line, where +1 indicates
// above the line, and -1 indicates below the line.
type Point struct {
	X     float64
	Y     float64
	Bias  float64
	Label int
	Color color.RGBA
}

// TrainingPoints produces a slice of points of length count having random
// positions within a square cartesian coordinate system of area = span * span,
// and origin at point (0, 0).
func TrainingPoints(count int, span float64, f func(float64) float64) []Point {
	points := make([]Point, count)

	for i := 0; i < count; i++ {
		p := randomPoint(span/2.0, span/2.0, f)
		points = append(points, p)
	}

	return points
}

// randomPoint creates a new point with -xMax < x < xMax and -yMax < y < yMax.
func randomPoint(xMax, yMax float64, f func(float64) float64) Point {
	p := Point{X: randomFloat(xMax), Y: randomFloat(yMax), Bias: 1.0}

	if p.Y > f(p.X) {
		p.Label = A
	} else {
		p.Label = B
	}

	return p
}

// randomFloat creates a random number within boundaries (-max, max) where max
// is a positive integer.
func randomFloat(max float64) float64 {
	return 2*rng.Float64()*max - max
}
