// Package neuralnet is a simple neural network inspired by Daniel Shiffman's
// youtube series on neural networks.
package neuralnet

import (
	"fmt"
	"os"
)

// A Classification is above the line, B Classification is below the line
const (
	A = 1
	B = -1
)

// Perceptron is a perceptron node in a neural network.
//
// The Perceptron Algorithm
//   1. For every input, multply that input by its weight
//   2. Sum all the weighted inputs.
//   3. Compute the output of the perceptron based on that sum passed through
//      an activation function (the sign of the sum).
type Perceptron struct {
	Weights      []float64
	sum          float64
	learningRate float64
}

// NewNode returns a new node with number of weights equal to inputCount. Each
// weight is set to a random real number in the range -1.0 < weight < 1.0.
func NewNode(inputCount int) *Perceptron {
	// Add one to the input count for the bias
	ic := inputCount + 1
	p := &Perceptron{
		Weights:      make([]float64, ic),
		learningRate: 0.1,
	}

	// Initialize weights randomly to the range -1.0 < weight < 1.0
	for i := 0; i < ic; i++ {
		p.Weights[i] = 2*rng.Float64() - 1
	}

	return p
}

// Guess gives the perceptron's guess as to the output
func (p *Perceptron) Guess(inputs []float64) (int, error) {
	if len(inputs) != len(p.Weights) {
		return 0, fmt.Errorf("input count, %d does not match weights count, %d",
			len(inputs), len(p.Weights))
	}

	// Calculate the weighted sum
	p.sum = 0.0
	for i, input := range inputs {
		p.sum += input * p.Weights[i]
	}

	// Invoke the activation function
	return p.sign(), nil
}

// Train adjusts perceptron weights by gradient descent
func (p *Perceptron) Train(points []Point) {
	for _, pt := range points {
		inputs := []float64{pt.X, pt.Y, pt.Bias}
		guess, err := p.Guess(inputs)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Calcualate difference between actual and estimate
		residual := pt.Label - guess

		// Tune all the weights
		for i := range p.Weights {
			p.Weights[i] += inputs[i] * float64(residual) * p.learningRate
		}
	}
}

// sign is the activation function
func (p *Perceptron) sign() int {
	if p.sum >= 0 {
		return A
	}

	return B
}

// PrintWeights shows the weights of the perceptron
func (p *Perceptron) PrintWeights() {
	for i := range p.Weights {
		fmt.Printf("weights[%d] = %.15f\n", i, p.Weights[i])
	}
}
