package view

import (
	"image/color"
	"math/rand"
	"time"

	"gitlab.com/mark.west1/codingtrain/neuralnet"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
)

// Colors of plotted points
var (
	Red   = color.RGBA{255, 0, 0, 255}
	Green = color.RGBA{0, 255, 0, 255}
	Blue  = color.RGBA{0, 0, 255, 255}
	Black = color.RGBA{0, 0, 0, 255}

	title       = "Coding Train 10 - Neural Networks"
	xAxisLabel  = "x-axis"
	yAxisLabel  = "y-axis"
	pngFile     = "images/points"
	lineWidth   = vg.Length(1.5)
	pointRadius = vg.Length(3)
	circle      = draw.CircleGlyph{}
	ring        = draw.RingGlyph{}
)

// ShowPoints produces a PNG plot of points spanning (span units x span units)
// on a square canvas width inches wide.
func ShowPoints(points []neuralnet.Point, width int, span float64) {
	now := time.Now()

	// Create plot
	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	p.Title.Text = title + " | " + now.Format("02 Jan 2006 15:04:05 MST")
	p.X.Label.Text = xAxisLabel
	p.Y.Label.Text = yAxisLabel
	p.Add(plotter.NewGrid())

	// Add line
	lpts := []struct{ X, Y float64 }{{0, 0}, {span, span}}
	line := &plotter.Line{
		XYs: lpts,
		LineStyle: draw.LineStyle{
			Color:    Green,
			Width:    lineWidth,
			Dashes:   nil,
			DashOffs: vg.Length(0),
		},
		ShadeColor: nil,
	}
	p.Add(line)

	// Add points above line
	above, err := plotter.NewScatter(makePlottable(points, neuralnet.A))
	if err != nil {
		panic(err)
	}
	above.GlyphStyle.Color = Red
	above.GlyphStyle.Radius = pointRadius
	above.GlyphStyle.Shape = circle
	p.Add(above)

	// Add points below line
	below, err := plotter.NewScatter(makePlottable(points, neuralnet.B))
	if err != nil {
		panic(err)
	}
	below.GlyphStyle.Color = Blue
	below.GlyphStyle.Radius = pointRadius
	below.GlyphStyle.Shape = circle
	p.Add(below)

	save(p, now, width)
}

// ShowTrainingPoints produces a PNG plot of points spanning (span units by
// span units) on a square canvas width inches wide.
func ShowTrainingPoints(points []neuralnet.Point, width int, span float64, f func(float64) float64) {
	now := time.Now()
	rand.Seed(now.UnixNano())

	// Create plot
	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	p.Title.Text = title + " | " + now.Format("02 Jan 2006 15:04:05 MST")
	p.X.Label.Text = xAxisLabel
	p.Y.Label.Text = yAxisLabel
	p.Add(plotter.NewGrid())

	x1 := -span / 2.0
	x2 := span / 2.0

	// Add line
	lpts := []struct{ X, Y float64 }{{x1, f(x1)}, {x2, f(x2)}}
	line := &plotter.Line{
		XYs: lpts,
		LineStyle: draw.LineStyle{
			Color:    Black,
			Width:    lineWidth,
			DashOffs: vg.Length(0),
		},
	}
	p.Add(line)

	// Add correct-guess points
	correctPts := makeTrainingPlottable(points, Green)
	correct, err := plotter.NewScatter(correctPts)
	if err != nil {
		panic(err)
	}
	correct.GlyphStyle.Color = Green
	correct.GlyphStyle.Radius = pointRadius
	correct.GlyphStyle.Shape = circle
	p.Add(correct)

	// Add incorrect-guess points
	incorrectPts := makeTrainingPlottable(points, Red)
	incorrect, err := plotter.NewScatter(incorrectPts)
	if err != nil {
		panic(err)
	}
	incorrect.GlyphStyle.Color = Red
	incorrect.GlyphStyle.Radius = pointRadius
	incorrect.GlyphStyle.Shape = circle
	p.Add(incorrect)

	save(p, now, width)
}

// makePlottable returns a list of (x, y) coordinates of points having Label
// value equal to label
func makePlottable(points []neuralnet.Point, label int) plotter.XYs {
	pts := make(plotter.XYs, 0)

	for i := range points {
		if points[i].Label == label {
			pts = append(pts, struct{ X, Y float64 }{points[i].X, points[i].Y})
		}
	}

	return pts
}

// makeTrainingPlottable
func makeTrainingPlottable(points []neuralnet.Point, color color.RGBA) plotter.XYs {
	pts := make(plotter.XYs, 0)

	for i := range points {
		if points[i].Color == color {
			pts = append(pts, struct{ X, Y float64 }{points[i].X, points[i].Y})
		}
	}

	return pts
}

// save the plot in a .png file with format "points_YYYYMMDD_HHMMSS.png".
func save(p *plot.Plot, now time.Time, width int) {
	filename := pngFile + "_" + now.Format("20060102_150405") + ".png"
	err := p.Save(vg.Length(width)*vg.Inch, vg.Length(width)*vg.Inch, filename)
	if err != nil {
		panic(err)
	}
}
