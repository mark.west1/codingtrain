package main

import (
	"fmt"
	"math"
	"os"

	"gitlab.com/mark.west1/codingtrain/neuralnet"
	"gitlab.com/mark.west1/codingtrain/view"
)

func main() {
	m0 := 0.3
	b0 := 0.2

	f := func(x float64) float64 { return m0*x + b0 }

	numTraining := 7500000
	numTest := 1000

	span := 2.0
	brain := neuralnet.NewNode(2)
	brain.Train(neuralnet.TrainingPoints(numTraining, span, f))

	correctGuess := 0
	incorrectGuess := 0

	testPoints := neuralnet.TrainingPoints(numTest, span, f)

	for i, p := range testPoints {
		guess, err := brain.Guess([]float64{p.X, p.Y, p.Bias})
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		if guess == p.Label {
			correctGuess++
			testPoints[i].Color = view.Green
		} else {
			incorrectGuess++
			testPoints[i].Color = view.Red
		}
	}

	m1 := -1.0 * brain.Weights[0] / brain.Weights[1]
	b1 := -1.0 * brain.Weights[2] / brain.Weights[1]

	fmt.Printf("  ACTUAL: y = %.3fx %s %.3f\n", m0, op(b0), math.Abs(b0))
	fmt.Printf("ESTIMATE: y = %.3fx %s %.3f\n", m1, op(b1), math.Abs(b1))
	fmt.Printf("for %d training points, correct guess percentage: %.2f (%d/%d)\n",
		numTraining, 100*float64(correctGuess)/float64(numTest), correctGuess, numTest)

	view.ShowTrainingPoints(testPoints, 7, span, f)
}

func op(b float64) string {
	if b >= 0 {
		return "+"
	}

	return "-"
}
